import pandas as pd
import numpy as np
# %matplotlib inline
# import matplotlib.pyplot as plt
from os import listdir
import keras
from keras.preprocessing import sequence
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM

from keras.optimizers import Adam
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from keras.utils import to_categorical

datasets = [
    'gaitdata-2kph.csv',
    'gaitdata-3-5kph.csv',
    'gaitdata-5kph.csv',
    'gaitdata-6-5kph.csv',
]

class model_builder():
    """
    Model builder class which returns our built model for training

    :params:
        None
    :return:
        None
    """
    def __init__(self, len_ts, number_of_ts):
        """
        :params:
            len_ts: length of time series input
            number_of_ts: number of time series input
        """
        self.len_ts = len_ts
        self.number_of_ts = number_of_ts
        self.model = self.build_model(self.len_ts, self.number_of_ts)

    def build_model(self, len_ts, number_of_ts):
        """
        Build single layer LSTM with 256 neurons and a single
        dense output layer

        This model classifies input against exisiting time series

        :params:
            len_ts: length of time series input
            number_of_ts: number of time series input
        """
        model = Sequential()
        timesteps = 1

        # create Model with 256 input neurons with shape len_ts x number_of_ts
        # model.add(LSTM(256, input_shape=(len_ts, number_of_ts)))
        model.add(LSTM(100, input_shape=(10, 1)))

        # add Dense output later with sigmoid activation function
        model.add(Dense(1, activation='sigmoid'))

        return model

class input_builder():
    """
    Class to build datasets and return valid numpy arrays
    """
    def __init__(self, datasets):
        self.datasets = datasets
        self.dataset_list = self.get_datasets()

    def get_datasets(self):
        """
        Gets datasets from csv files
        """
        dataset_list = list()

        timesteps = 10
        dim = 1

        for dataset in self.datasets:
            df = pd.read_csv('datasets/{}'.format(dataset))
            samples = len(df)
            df = df['emg'][:samples-1]
            df = df.values
            df.shape = (int(samples/timesteps),timesteps,dim)
            dataset_list.append(df)

        return dataset_list

    def build_input(self):
        """
        Build input array for model
        """
        # return np.stack(self.dataset_list)
        return self.dataset_list

# class dataset_preparer():
#     """
#     Class to build train, validation, test datasets
#     """
#     def __init__(self, input_sets):
#         self.input_sets = input_sets
#
#     def get_test_train_validation():
#         train =
#         train = [final_seq[i] for i in range(len(groups)) if (groups[i]==2)]
#         validation = [final_seq[i] for i in range(len(groups)) if groups[i]==1]
#         test = [final_seq[i] for i in range(len(groups)) if groups[i]==3]

# Callback to display the target and prediciton
# testmodelcb = keras.callbacks.LambdaCallback(on_epoch_end=testmodel)

input = input_builder(datasets).build_input()
new_model = model_builder(len(input[0]), len(input))
new_model.model.summary()
model = new_model.model

input = np.concatenate(tuple(i for i in input))

output1 = np.full((265,), 2)
output2 = np.full((265,), 3.5)
output3 = np.full((265,), 5)
output4 = np.full((265,), 6.5)

y = np.concatenate((output1, output2, output3, output4))

adam = Adam(lr=0.001)
chk = ModelCheckpoint('best_model.pkl', monitor='val_acc', save_best_only=True, mode='max', verbose=1)
model.compile(loss='mse', optimizer=adam, metrics=['acc'])
model.fit(input, y, epochs=200, batch_size=32, shuffle=False, verbose=2)
